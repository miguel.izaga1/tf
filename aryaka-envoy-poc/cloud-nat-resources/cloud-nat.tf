data "terraform_remote_state" "vpc_state" {
  backend = "gcs"

  config = {
    bucket = "aryaka-perf-poc-state-bucket"
    prefix = "vpc/state"
  }
}

data "terraform_remote_state" "cr_state" {
  backend = "gcs"

  config = {
    bucket = "aryaka-perf-poc-state-bucket"
    prefix = "cloud-router/state"
  }
}

module "cloud-nat" {
  source  = "terraform-google-modules/cloud-nat/google"
  version = "5.0.0"

  project_id    = var.project
  region        = var.region
  router  = data.terraform_remote_state.cr_state.outputs.cr_name
  enable_dynamic_port_allocation     = true
  min_ports_per_vm                   = 32768
  max_ports_per_vm                   = 65536
  tcp_time_wait_timeout_sec          = 60


}
