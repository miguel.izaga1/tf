package main

import (
	"fmt"
	"os"
)

func generateDomains(inputString, proxyIP string, numDomains int) []string {
	domains := make([]string, numDomains)
	for i := 1; i <= numDomains; i++ {
		httpsDomain := fmt.Sprintf("https://%s-%d.com:443/index.html", inputString, i)
		httpDomain := fmt.Sprintf("http://t1user1:t1user1@%s:10001", proxyIP)
		domains[i-1] = httpsDomain + " " + httpDomain
	}
	return domains
}

func writeDomainsToFile(domains []string, filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	for _, domain := range domains {
		_, err := file.WriteString(domain + "\n")
		if err != nil {
			return err
		}
	}

	return nil
}

func main() {
	var inputString, proxyIP string
	var numDomains int

	fmt.Println("Enter input string:")
	fmt.Scanln(&inputString)

	fmt.Println("Enter proxy IP:")
	fmt.Scanln(&proxyIP)

	fmt.Println("Enter number of domains:")
	fmt.Scanln(&numDomains)

	domains := generateDomains(inputString, proxyIP, numDomains)

	fmt.Println("Enter filename to save domains:")
	var filename string
	fmt.Scanln(&filename)

	err := writeDomainsToFile(domains, filename)
	if err != nil {
		fmt.Println("Error writing to file:", err)
		return
	}

	fmt.Println("Domains saved to", filename)
}

