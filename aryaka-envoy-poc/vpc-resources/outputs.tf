output "vpc_network" {
  description = "The name of the VPC network"
  value       = module.network.network_name
}

output "subnets" {
  description = "The subnetes inside the VPC"
  #value = [for subnet_name, _ in module.network.subnets : split("/", subnet_name)[1]]
  value = values(module.network.subnets)[0].name

}

#output "secondary_ip_range_names" {
#  description = "Names of secondary IP ranges for each subnet inside the VPC"
#  value = flatten([
#    for subnet_name, subnet in module.network.subnets : [
#      for range in subnet.secondary_ip_range : range.range_name
#    ]
#  ])
#}

output "secondary_ip_range_for_pods" {
  description = "Secondary IP range for pods inside the VPC"
  value       = values(module.network.subnets)[0].secondary_ip_range[0].ip_cidr_range
}

output "secondary_ip_range_name_for_pods" {
  description = "Secondary IP range for pods inside the VPC"
  value       = values(module.network.subnets)[0].secondary_ip_range[0].range_name
}

output "secondary_ip_range_for_services" {
  description = "Secondary IP range for services inside the VPC"
  value       = values(module.network.subnets)[0].secondary_ip_range[1].ip_cidr_range
}

output "secondary_ip_range_name_for_services" {
  description = "Secondary IP range for services inside the VPC"
  value       = values(module.network.subnets)[0].secondary_ip_range[1].range_name
}
