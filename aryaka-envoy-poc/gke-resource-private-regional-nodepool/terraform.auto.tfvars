project_id                = "incubation-408916"
region                    = "us-central1"
remove_default_node_pool  = true
deletion_protection       = false
default_max_pods_per_node = 50
enable_shielded_nodes     = true
node_pools = [
  {
    name                      = "aryaka-poc-node-pool-1"
    machine_type              = "n2-standard-4"
    min_cpu_platform          = "Intel Ice Lake"
    node_locations            = "us-central1-a,us-central1-b,us-central1-c"
    min_count                 = 1
    max_count                 = 12
    local_ssd_count           = 0
    spot                      = false
    local_ssd_ephemeral_count = 0
    disk_size_gb              = 100
    disk_type                 = "pd-standard"
    image_type                = "COS_CONTAINERD"
    enable_gcfs               = false
    enable_gvnic              = false
    logging_variant           = "DEFAULT"
    auto_repair               = true
    auto_upgrade              = true
    preemptible               = false
    initial_node_count        = 1
    enable_secure_boot        = true
  },
]
