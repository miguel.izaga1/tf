output "cr_name" {
  description = "The name of the Cloud Router"
  value       = module.cloud_router.router.name
}
