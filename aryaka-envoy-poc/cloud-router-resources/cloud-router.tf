data "terraform_remote_state" "vpc_state" {
  backend = "gcs"

  config = {
    bucket = "aryaka-perf-poc-state-bucket"
    prefix = "vpc/state"
  }
}


module "cloud_router" {
  source  = "terraform-google-modules/cloud-router/google"
  version = "6.0.2"
  name    = var.name
  project = var.project
  network = data.terraform_remote_state.vpc_state.outputs.vpc_network
  region  = var.region

}
